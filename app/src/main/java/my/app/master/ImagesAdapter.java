package my.app.master;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.ArrayList;
import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.MyViewHolder> {
    ArrayList <String> uriStr = new ArrayList<String>();
    //private ArrayList<DataModel> dataSet;

    private List<Upload> adpURLs;

    public ImagesAdapter(List<Upload>urllists) {
        adpURLs=urllists;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_lay, parent, false);
        //view.setOnClickListener(MainActivity.myOnClickListener);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        final ImageView imageView = holder.imageViewIcon;
        final ImageView imageNoIcon = holder.imgNoIcon;
        final TextView txtVwBar = holder.txtBarcodeview;
        //URLList uri = adpURLs.get(listPosition);
        //Log.e("Display", "Outside DISP");
        for (int i =0; i< adpURLs.size();i++){
            //Log.e("Display", adpURLs.get(i).toString());
            //String fileURL = adpURLs.get(i).toString();
            String fileURL;
            String txtBarcode;
            fileURL= adpURLs.get(listPosition).fileName;
            txtBarcode=adpURLs.get(listPosition).BarCodeValue;
            if (fileURL!=null && txtBarcode!=null) {
                txtVwBar.setText(txtBarcode);
                GlideApp.with(imageView.getContext()).load(fileURL).thumbnail(0.25f).into(imageView);
                //imageView.setAdjustViewBounds(true);
                //imageView.setVisibility(View.);
                //imageNoIcon.setVisibility(View.GONE);
                //imageNoIcon.setLayoutParams(new RecyclerView.LayoutParams(0,0));
            }
            else if (txtBarcode!=null) {
                GlideApp.with(imageNoIcon.getContext()).load(fileURL).thumbnail(0.25f).error(GlideApp.with(imageNoIcon.getContext()).load(R.drawable.null_img))
                        .into(imageNoIcon);
                txtVwBar.setText(txtBarcode);
                //imageNoIcon.setAdjustViewBounds(true);
                //imageView.setVisibility(View.GONE);
                // imageView.setLayoutParams(new RecyclerView.LayoutParams(0,0));

            }

        }
        //final ImageView finalImageViewIcon = imageViewIcon;


    }

    @Override
    public int getItemCount() {
        return adpURLs.size();
        //return 3;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView imageViewIcon,imgNoIcon;
        TextView txtBarcodeview;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.myImg);
            this.imgNoIcon = (ImageView) itemView.findViewById(R.id.NoImg);
            this.txtBarcodeview = (TextView) itemView.findViewById(R.id.myCodeText);

        }
    }









}
