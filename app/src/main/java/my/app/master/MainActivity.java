package my.app.master;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PICK_IMAGE_REQUEST = 1;
     ImageButton mButtonChooseImage;
     ImageButton mButtonUpload;
     ImageButton scanBtn;
    private TextView mTextViewShowUploads;
    private EditText mEditTextFileName;
    private ImageView mImageView;
    private ProgressBar mProgressBar;
    private String OwnerId;
    private Uri mImageUri;
    private FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;
    private StorageReference mStorageRef;
    private CollectionReference mDatabaseRef;
    private StorageTask mUploadTask;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //remove status bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mButtonChooseImage = findViewById(R.id.button_choose_image);
        mButtonUpload = findViewById(R.id.button_upload);
        mTextViewShowUploads = findViewById(R.id.show_uploads);
        mEditTextFileName = findViewById(R.id.edit_text_file_name);
        mImageView = findViewById(R.id.image_view);
        mProgressBar = findViewById(R.id.progress_bar);

        FirebaseUser userInfo = FirebaseAuth.getInstance().getCurrentUser();
        OwnerId=userInfo.getUid();
        mStorageRef = FirebaseStorage.getInstance().getReference("images/"+OwnerId);
        mDatabaseRef = FirebaseFirestore.getInstance().collection("fileInfo");

        scanBtn=findViewById(R.id.scanBtn);
        scanBtn.setOnClickListener(this);

        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
        //mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
          //  @Override
           // public void onClick(View v) {
               // Toast.makeText(getApplicationContext(),"You download is resumed",Toast.LENGTH_LONG).show();
           // }
       // });

        mButtonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUploadTask != null && mUploadTask.isInProgress()) {
                    Toast.makeText(MainActivity.this, "Upload in progress", Toast.LENGTH_SHORT).show();
                } else {
                    uploadFile();
                }
            }
        });

        mTextViewShowUploads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImagesActivity();
            }
        });

      /*  mTextViewShowUploads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImagesActivity();
            }
        });*/

        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);

        if (isFirstRun) {
            //show sign up activity
            startActivity(new Intent(MainActivity.this, SignUpActivity.class));
            //Toast.makeText(MainActivity.this, "Run only once", Toast.LENGTH_LONG).show();
        }


        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                .putBoolean("isFirstRun", false).commit();
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null)  {
            mImageUri = data.getData();

            Picasso.get().load(mImageUri).into(mImageView);

        }
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() != null) {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                final String scannedVal = result.getContents();
                builder.setMessage(scannedVal);
                builder.setTitle("Scanned Value");

                FirebaseFirestore db = FirebaseFirestore.getInstance();
                Map<String, Object> user = new HashMap<>();
                //user.put("values", test);
                Upload upl = new Upload();
                upl.Status= "Done";
                upl.Summary="Scan";
                upl.BarCodeValue=scannedVal;
                FirebaseUser userInfo = FirebaseAuth.getInstance().getCurrentUser();
                OwnerId=userInfo.getUid();
                //Toast.makeText(this, "Owner  " + this.OwnerId, Toast.LENGTH_SHORT).show();
                upl.owner=this.OwnerId;
                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                Calendar cal = Calendar.getInstance();
                Date date = cal.getTime();
                String todaysdate = dateFormat.format(date);
                upl.DOP=todaysdate;
                mDatabaseRef.add(upl).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) { String TAG = "FileUpload";
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) { String TAG = "FileUpload";
                                Log.w(TAG, "Error adding document", e);
                            }
                        });

                builder.setPositiveButton("Scan Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        scanCode();

                    }
                }).setNegativeButton("Finish", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

            }
            else {
                //Toast.makeText(this, "No Result", Toast.LENGTH_LONG).show();
            }

        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
    private void uploadFile() {
        
        try {
            if (mImageUri != null) {
                StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                        + "." + getFileExtension(mImageUri));
                mUploadTask = fileReference.putFile(mImageUri)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        mProgressBar.setProgress(0);
                                    }
                                }, 500);
                                Toast.makeText(MainActivity.this, "Upload successful", Toast.LENGTH_LONG).show();
                                mImageView.setImageDrawable(null);
                                Upload upl = new Upload();
                                //Toast.makeText(MainActivity.this, "Toasting Upload successful", Toast.LENGTH_LONG).show();
                                upl.Status = "Inprogress";
                                upl.Summary = mEditTextFileName.getText().toString().trim();
                                upl.fileName = taskSnapshot.getUploadSessionUri().toString();
                                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
                                Calendar cal = Calendar.getInstance();
                                Date date = cal.getTime();
                                //Toast.makeText(MainActivity.this, "Date successful", Toast.LENGTH_LONG).show();
                                String todaysdate = dateFormat.format(date);
                                upl.DOP = todaysdate;

                                // ,"4uB2aJzkM8XC0Ah7ahP7CAI0A4I2",null,mEditTextFileName.getText().toString().trim());
                                mDatabaseRef.add(upl);
                                //Toast.makeText(MainActivity.this, "DB successful", Toast.LENGTH_LONG).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                //Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                                mProgressBar.setProgress((int) progress);
                            }
                        });
            } else {
                Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception e){

            Log.println((Integer)1,"Exception value",e.getMessage());
                    }
    }

    private void openImagesActivity() {
        //Toast.makeText(this, "Testings", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ImagesActivity.class);
        startActivity(intent);

    }



    public void onClick(View v){
        scanCode();

    }

    private void scanCode(){
        IntentIntegrator integrator=new IntentIntegrator(this);
        integrator.setCaptureActivity(captureAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning Code");
        integrator.initiateScan();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mnuInflater = getMenuInflater();
        mnuInflater.inflate(R.menu.nav_menu, menu);
        //Toast.makeText(this, "Menu Initiated", Toast.LENGTH_SHORT).show();
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       // Toast.makeText(this, "Testing Menu " +item.getItemId(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        switch (item.getItemId()) {


                case R.id.contact:
                    intent = new Intent(this, Contact.class);
                    startActivity(intent);
                    return true;


                case R.id.uploads:
                     intent = new Intent(this, ImagesActivity.class);
                    startActivity(intent);
                    return true;


                case R.id.faq:
                     intent = new Intent(this, Faq.class);
                    startActivity(intent);
                    return true;


                case R.id.aboutus:
                    intent = new Intent(this, About.class);
                    startActivity(intent);
                    return true;


            case R.id.signOut:
                //Toast.makeText(MainActivity.this, "SignOut", Toast.LENGTH_LONG).show();
                signOut();
                Intent signUpAct = new Intent(this,SignUpActivity.class);
                startActivity(signUpAct);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }





    public void signOut() {
        // Firebase sign out
        FirebaseAuth.getInstance().signOut();
        Intent signUpAct = new Intent(this,SignUpActivity.class);
        signUpAct.putExtra("finish", true);
        signUpAct.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(signUpAct);
        finish();


    }


}
